package bridge.util;

import javax.activation.MimeType;
import java.io.*;
import java.util.Properties;
import java.util.UUID;

public class FileUtil {

	public static String getFileContent(String filename)  {
		StringBuilder sb = new StringBuilder();
		try{
		String FolderPath = System.getProperty("user.dir");
		File fileToRead = new File(FolderPath + "//TestData//" + filename);
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(fileToRead));
		try {

			line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
		} finally {
			br.close();
		}}
		catch(Exception e){

		}
 return sb.toString();
	}
	public static String RandomStringUUID() {
           UUID uuid = UUID.randomUUID();
			return uuid.toString();
		}

	public static  String createReportFile() {
		long unixTime=System.currentTimeMillis()/1000;
		String filepath=System.getProperty("user.dir")+"//Report"+unixTime+".html";
		File f= new File(filepath);
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return  filepath;
	}
	
	
	public static String replaceText(String text, String repalcechar,String reaplcenewChar) {
		return text.replace(repalcechar, reaplcenewChar);
	}
	// createfile
	// unixtimestamp

	public static String getProperty(String key) {
		InputStream input;
		String text="";
		try {
			String location=System.getProperty("user.dir");
			input = new FileInputStream(location+"//config.properties");
			Properties prop = new Properties();
			prop.load(input);
		text= prop.getProperty(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
return text;
		// get the property value and print it out
		 
	}
	public static void setproperty(String strKey,String strValue)  {
		try{
			String strFileName=System.getProperty("user.dir")+"//config.properties";
			File f = new File(strFileName);
			  Properties props=new Properties();
			if(f.exists()){
				FileInputStream in=new FileInputStream(f);
				props.load(in);
				props.setProperty(strKey, strValue);
				props.store(new FileOutputStream(strFileName),null);
				in.close();
			}
			else{
				System.out.println("File not Found");
			}
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}


}
