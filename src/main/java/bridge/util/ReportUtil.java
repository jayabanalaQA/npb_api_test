package bridge.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ReportUtil {
    String testname="SuiteName";
    static ExtentReports extent = new ExtentReports();
    static   ExtentTest  logger;
    static  ExtentHtmlReporter reporter=new ExtentHtmlReporter(System.getProperty("user.dir")+"/reports/report.html");;


    public void createTest(String testCaseName)  {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            String hostname = ip.getHostName();
            extent.setSystemInfo("IP", ""+ip);
            extent.setSystemInfo("Hostname", hostname);
            extent.setSystemInfo("Project Name Location", System. getProperty("user.dir"));
            reporter.config().getChartVisibilityOnOpen();
            reporter.config().setDocumentTitle("Symphony NPB_API");
            reporter.config().setReportName("Regression Test");
            reporter.config().setTheme(Theme.STANDARD);
            extent.attachReporter(reporter);
            logger = extent.createTest(testCaseName);

        }
        catch(Exception e){
            closeReport();
        }
    }

   public void addLoggerTextbold(Status status,String Desciption){

        if(status==Status.FAIL){
            logger.log(Status.FAIL, MarkupHelper.createLabel(Desciption +"Test Step Failed", ExtentColor.RED));
       }else{
            logger.log(Status.FAIL, MarkupHelper.createLabel(Desciption+"Test Step Failed", ExtentColor.GREEN));
        }

   }
    public void addLogger(Status status,String Desciption){
            logger.log(status, "Desciption ");

    }
    public void closeReport(){
        extent.close();
        extent.flush();
    }


}
