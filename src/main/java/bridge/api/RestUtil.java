package bridge.api;

import bridge.util.FileUtil;
import bridge.util.ReportUtil;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.jsonpath.JsonPath;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import com.jayway.jsonpath.DocumentContext;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpEntity;
import org.apache.http.protocol.HTTP;
import org.junit.Assert;

import java.net.HttpURLConnection;
import java.util.Map;


public class RestUtil {

	//config.propertiea

	ReportUtil report= new ReportUtil();


    public void verifyStatusCode(Response r,String expectedCode) {
		try {
			if (r.statusCode() == Integer.parseInt(expectedCode)) {
				report.addLogger(Status.PASS, "Sucessfully Verified status");

			} else {
				report.addLogger(Status.FAIL, "FAil to  Verify status");
			}
		} catch (Exception e) {
			report.addLogger(Status.FAIL, "FAil to  Verify status, Following Exceoption occured " + e.getLocalizedMessage());
		}
	}


    public Response ExecuteApi(RequestType method, String EndpointURI, String payload, String statuscode, Map header, String contentType){
		Response r = null;
    	try{
    	RestAssured.baseURI =   EndpointURI;
		RestAssured.useRelaxedHTTPSValidation();

		 if(method==RequestType.POST){
		r=RestAssured.given().headers(header).contentType(contentType).body(payload)
				.when().post().then().extract().response();
		 }
		 else{
		 	r=RestAssured.given().headers(header).contentType(contentType).get().then().extract().response();
		 }
		 if(r!=null){
			 ResponseBody body=r.getBody();
			 String jsonText = body.asString();
			 report.addLogger(Status.PASS, "Sucessfully Executed API and Reponse is "+jsonText);
		 }else{
			 report.addLogger(Status.FAIL, "Fail to Execute API");
		 }
			return r;
    	}
		catch (Exception e) {
			report.addLogger(Status.FAIL, "FAil to  Verify status, Following Exceoption occured " + e.getLocalizedMessage());
		}
 return r;

	}

public void verifyNodeValuematches(Response r, String jsonPath,String expectedText){
    	try{
    	String actual=getNodeValue(r,jsonPath);
    	if(actual.equalsIgnoreCase(expectedText)){
			report.addLogger(Status.PASS, "Value matched with expected");
			Assert.assertTrue("Value matches",true);
		}else{
			report.addLogger(Status.FAIL, "Value not matched with expected "+expectedText);
    		Assert.assertTrue( "Value not matches",false);
		}}
		catch (Exception e) {
			report.addLogger(Status.FAIL, "FAil to  Verify the node value, Following Exceoption occured " + e.getLocalizedMessage());
		}
}


	public String getNodeValue(Response r, String jsonPath) {
		ResponseBody body=r.getBody();
		String jsonText = body.asString();
		try {
			if (jsonText.trim().isEmpty()) {
				System.out.print("JSON text cannot be empty.");
			}
			if (jsonPath.trim().isEmpty()) {
				System.out.print("JSON node path cannot be empty.");
			}
			Object json = JsonPath.parse(jsonText).read(jsonPath);
			if (json == null) {
				return null;
			}
			if (json.toString().isEmpty()) {
				return "";
			}
System.out.print(json.toString());
			return json.toString();
		} catch (Exception ex) {
			System.out.print(ex.toString());
		}
		return jsonPath;
	}

}
