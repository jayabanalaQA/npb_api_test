package bridge.api;

public enum RequestType {
    POST, GET, DELETE, PUT;
}
