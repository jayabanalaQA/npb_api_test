
Feature: Configuration
 As I want to run the api configuration API's

 Background:
 Given I am executing the Configuration api

  Scenario: Generate Token
    When User Generates a Token
    Then user should be able to see the response and status code "200"

  Scenario: Create Client
    When User creates a client
    Then user should be able to see the response and status code "200"

  Scenario: Create POS instance
    When User creates a POS instance
    Then user should be able to see the response and status code "200"

  Scenario: User Syncs Data
    When User Syncs Data
    Then user should be able to see the response and status code "200"


  Scenario: Get information about the current POS instance
    When User triggers an api to get discovery in the configuration
    Then user should be able to see the response and status code "200"

  Scenario: Get predefined list of features and points which of them are supported by active POS
    When User triggers an api to get configuration features
    Then user should be able to see the response and status code "200"

  Scenario: Gets configuration elements required by POS
    When User triggers an api to get configuration elements
    Then user should be able to see the response and status code "200"





