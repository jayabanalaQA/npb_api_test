package bridge.steps;

import bridge.api.RequestType;
import bridge.api.RestUtil;
import bridge.util.FileUtil;
import bridge.util.ReportUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

import java.util.HashMap;

public class ConfigiurationSteps {

    static RestUtil util= new RestUtil();
    static Response response;


    @Given("^I am executing the Configuration api$")
    public void iAmExecutingTheConfigurationApi() {

    }


    @When("^User triggers an api to get discovery in the configuration$")
    public void userTriggersAnApiToGetDiscoveryInTheConfiguration() {
        HashMap<String,String> header= new HashMap<String,String>();
        header.put("accept", "text/plain");
        String baseuri = FileUtil.getProperty("url");
        response=util.ExecuteApi(RequestType.GET,
                baseuri+"/configuration/discovery",
                "",
                "200", header,
                "application/json-patch+json");


    }

    @Then("^user should be able to see the response and status code \"([^\"]*)\"$")
    public void userShouldBeAbleToSeeTheResponseAndStatusCode(String statusCode) throws Throwable {
        util.verifyStatusCode(response, statusCode);

    }

    @When("^User triggers an api to get configuration features$")
    public void userTriggersAnApiToGetConfigurationFeatures() {
        HashMap<String,String> header= new HashMap<String,String>();
        header.put("accept", "text/plain");
        String baseuri = FileUtil.getProperty("url");
        response=util.ExecuteApi(RequestType.GET,
                baseuri+"/configuration/features",
                "",
                "200",header,
                "application/json-patch+json");

    }

    @When("^User triggers an api to get configuration elements$")
    public void userTriggersAnApiToGetConfigurationElements() {
        HashMap<String, String> header= new HashMap<String,String>();
        header.put("accept", "text/plain");
        String baseuri = FileUtil.getProperty("url");
        response=util.ExecuteApi(RequestType.GET,
                baseuri+"/configuration/elements",
                "",
                "200", header,
                "application/json");
    }

    @When("^User Generates a Token$")
    public void userGeneratesAToken() {

        HashMap<String,String> header= new HashMap<String,String>();
        header.put("Authorization", "Basic YWRtaW46dGVzdA==");
        header.put("accept", "*/*");
         response=util.ExecuteApi(RequestType.POST,
                "https://microservice.npb.lineten.io/v2/token",
                "",
                "200",header,
                "application/json");
        String toekn=util.getNodeValue(response,"$.Data.token");
        FileUtil.setproperty("token",toekn);
    }

    @When("^User creates a client$")
    public void userCreatesAClient() {
        String token=FileUtil.getProperty("token");
     String payloadtext=FileUtil.replaceText( FileUtil.getFileContent("CreateClient.json"),"{{client_code}}",FileUtil.RandomStringUUID());
       HashMap<String,String> header= new HashMap<String,String>();
        header.put("Authorization", "Bearer "+token);
        header.put("accept", "*/*");
        response=util.ExecuteApi(RequestType.POST,
                "https://microservice.npb.lineten.io/v1/referencedata/client",
                payloadtext,
                "200",header,
                "application/json");
         String toekn=util.getNodeValue(response,"$.Data.admin.user");
        FileUtil.setproperty("adminuser",toekn);
         toekn=util.getNodeValue(response,"$.Data.api.user");
        FileUtil.setproperty("apiuser",toekn);
    }

    @When("^User creates a POS instance$")
    public void userCreatesAPOSInstance() {

        String token=FileUtil.getProperty("token");
        String payloadtext= FileUtil.getFileContent("CreatePOS.json");
        HashMap<String,String> header= new HashMap<String,String>();
        header.put("Authorization", "Bearer "+token);
        header.put("accept", "*/*");
        response=util.ExecuteApi(RequestType.POST,
                "https://microservice.npb.lineten.io/v1/referencedata/clientpos",
                payloadtext,
                "200",header,
                "application/json");
        String toekn=util.getNodeValue(response,"$.Data.id");
        FileUtil.setproperty("posid",toekn);
    }

    @When("^User Syncs Data$")
    public void userSyncsData() {
        String token=FileUtil.getProperty("token");
        String posid=FileUtil.getProperty("posid");
          HashMap<String,String> header= new HashMap<String,String>();
        header.put("Authorization", "Bearer "+token);
        header.put("accept", "*/*");
        response=util.ExecuteApi(RequestType.GET,
                "https://microservice.npb.lineten.io//v1/referencedata/clientpos/"+posid+"/sync",
                "",
                "200",header,
                "application/json");

    }
}
