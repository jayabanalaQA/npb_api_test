package bridge.runner;

import bridge.util.ReportUtil;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
		  features = {"classpath:cucumber/Configuration.feature","classpath:cucumber/Test.feature"},
		  glue = "bridge.steps",
		monochrome = true,
format = "json:target/cucumber-json-report.json"

)

public class RunTest
	{

		static ReportUtil report= new ReportUtil();


	@BeforeClass
public static void before(){

		report.createTest("New Test");
	}

	@AfterClass
	public static void after(){
		report.closeReport();
	}

	}